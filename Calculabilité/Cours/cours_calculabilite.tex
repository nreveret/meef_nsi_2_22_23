\documentclass[a4paper, 11pt]{article}

\title{
  \rule{\linewidth}{0.8pt}
  \Large{\scshape{MEEF - NSI}}

  \vspace{0.5cm}
  \large{\textit{Université Catholique de l'Ouest}}

  \vspace{0.5cm}
  \Large{\textbf{Calculabilité - Décidabilité}}

  \rule[10pt]{\linewidth}{0.8pt}
    }
\author{N. Revéret}
\date{\today}

%-------------------------------------------------------
% Les packages
%-------------------------------------------------------
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
% \usepackage{multicol} % les colonnes
\usepackage[french]{babel}
\addto\captionsfrench{\def\tablename{Tableau}}
\addto\captionsfrench{\def\figurename{Figure}}
\usepackage{lmodern}
\usepackage{hyperref}     % liens hypertextes
\usepackage{graphicx}   % insertion d'images
\usepackage{booktabs}     % Les tableaux
\usepackage{amssymb}      % symboles maths
\usepackage{amsmath}      % symboles maths
% \usepackage{blindtext}  % faux texte
\usepackage{float}        % positionnement des tableaux et autres flottants
\usepackage{multirow}     % lignes fusionnées dans les tableaux 
\usepackage{lastpage}     % compter les pages
\usepackage{soul}         % surlignement du texte
\usepackage{stmaryrd}     % doubles crochets
%-------------------------------------------------------
% Figure geogebra -> tikz
%-------------------------------------------------------
\usepackage{pgfplots}
\pgfplotsset{compat=1.15}
\usepackage{mathrsfs}
\usetikzlibrary{shapes.arrows,chains,arrows, positioning, snakes, calc, shapes.misc, decorations.shapes, decorations.markings, backgrounds}
\usepackage{tikz}
%-------------------------------------------------------
% Dimension de la page
%-------------------------------------------------------
\usepackage[a4paper, total={17cm, 25cm}, includehead, headsep=12pt]{geometry} % taille de la page et marges

%-------------------------------------------------------
% Longueurs personnelles
%-------------------------------------------------------
\setlength{\parskip}{10pt} % 6pt par défaut
\setlength{\parindent}{15pt}

%-------------------------------------------------------
% Les entêtes et pied de pages
%-------------------------------------------------------
\usepackage{fancyhdr}
\pagestyle{fancy}
\fancyhf{}
\fancyhead[L]{\scriptsize MEEF - NSI}
\fancyhead[R]{\scriptsize Calculabilité - Décidabilité}
\fancyfoot[C]{\scriptsize - $\thepage$/\pageref{LastPage} -}
\renewcommand{\headrulewidth}{0.3pt}
\renewcommand{\footrulewidth}{0.3pt}

%-------------------------------------------------------
% La numérotation des questions et des listes
%-------------------------------------------------------
\usepackage[shortlabels]{enumitem}
\setlist[itemize,1]{label=\textbullet} % le label des listes de niveau 1 est un bullet
\setlist[itemize,2]{label=$\circ$} % le label des listes de niveau 2 est un bullet vide
\setlist[itemize,3]{label=$\diamond$} % le label des listes de niveau 3 est un diamant


%-------------------------------------------------------
% Dossiers contenant les images
%-------------------------------------------------------
\graphicspath{ 
    {images/}
}

%-------------------------------------------------------
% L'environnement Exercice
%-------------------------------------------------------
\newcounter{exercice}
\newenvironment{Exercice}[1][]{
    \refstepcounter{exercice}
    \par
    \noindent
    {\large
    \underline{\textit{Exercice~\theexercice~:}}~#1}
    % \vspace{\parskip}
    \par}
    {\vspace{1em}}

%-------------------------------------------------------
% L'environnement Question
%-------------------------------------------------------
\newcounter{question}[exercice]
\newenvironment{Question}{
    \refstepcounter{question}
    \begin{list}{}{%
      \setlength{\labelindent}{\parindent}
      \setlength{\leftmargin}{2\parindent}
      \setlength{\parsep}{\parskip} % Espacement vertical des sous-paragraphes
    }
    \item[\medskip\textbf{\thequestion.}]}
    {\end{list}}

%-------------------------------------------------------
% L'environnement subQuestion
%-------------------------------------------------------
\newcounter{subquestion}[question]
\newenvironment{subQuestion}{
    \refstepcounter{subquestion}
    \begin{list}{}{%
      \setlength{\leftmargin}{12pt}
      \setlength{\labelwidth}{8pt}
      \setlength{\labelsep}{4pt}
      \setlength{\listparindent}{0em}
      \setlength{\itemindent}{0em}
      \setlength{\parsep}{\parskip} % Espacement vertical des sous-paragraphes
    }
    \item[\medskip\textbf{\alph{subquestion}.}]}
    {\end{list}}
  

%-------------------------------------------------------
% Mise en forme du code
%-------------------------------------------------------
\usepackage[newfloat=false]{minted}
\setminted{autogobble, tabsize=4, breaklines, breakafter=_, linenos, numberblanklines=false, resetmargins, frame=leftline}
% Environnement python code
\newminted{python}{} % permet d'utiliser \begin{pythoncode} pour écrire du python

% Environnement mintinlinepy
\newmintinline[mintinlinepy]{python}{breaklines, breakafter=_} % permet d'utiliser \py{...} pour écrire du python en ligne
\newcommand{\py}{\mintinlinepy} % on peut utilise \py{code...}
    
% Environnement textcode
\newminted{text}{} % permet d'utiliser \begin{textcode} pour écrire du code en français

% Environnement textcode
\newmintinline[mintinlinetext]{text}{breaklines, breakafter=_} % permet d'utiliser \mintinlinetext{...} pour écrire du code en ligne en français


% Environnement de code sur plusieurs pages
\usepackage{caption}
\newenvironment{longcode}{\captionsetup{type=listing}}{}

%-------------------------------------------------------
% Commandes personnelles
%-------------------------------------------------------
\newcommand{\python}{\texttt{Python}}
\renewcommand{\thesection}{\Alph{section}.}
\renewcommand{\thesubsection}{\Alph{section}.\arabic{subsection}.}
%-------------------------------------------------------

% Le document
%-------------------------------------------------------
\begin{document}


%-------------------------------------------------------
% La page de titre
%-------------------------------------------------------
\maketitle

\vfill

%-------------------------------------------------------
% La table des matières
%-------------------------------------------------------
\setcounter{tocdepth}{3}
\tableofcontents
\newpage

Les questions de calculabilité et de décidabilité sont parmi les plus théoriques du programme de NSI.
Il est hors de question de les aborder de front ni de manière approfondie avec des lycéens.
Les objectifs cités dans le programme officiel sont les suivants :
\begin{itemize}
  \item \textit{montrer l'universalité et les limites de la notion de calculabilité};
  \item \textit{comprendre que la calculabilité ne dépend pas du langage de programmation utilisé};
  \item \textit{montrer, sans formalisme théorique, que le problème de l'arrêt est indécidable}.
\end{itemize}

Le présent exposé vise à les traiter sans pour autant trop rentrer dans les détails.

\section{Machine(s) de Turing}

En 1936, Alan Turing publie l'article \og \textit{On Computable Numbers, with an Application to the Entscheidungsproblem} \fg\ dans lequel,
outre la résolution du \textit{problème de la décision}, le \textit{Entscheidungsproblem} cité dans l'article, il présente le modèle d'une machine de calcul universelle qui, malgré sa simplicité,
permet de modéliser le fonctionnement de nos ordinateurs.

Rappelons au passage qu'il n'a alors que 24 ans.

\subsection{Machine de Turing}

Avec sa machine, Turing vise à modéliser les actions possibles par une personne qui effectue des calculs sur une feuille de papier.

\begin{figure}[H]
  \begin{center}
    \includegraphics[width=0.3\linewidth]{history-of-computing}
    \caption{Les calculs à l'époque de Turing}
  \end{center}
\end{figure}

La machine qu'il décrit est ainsi composée de deux parties :
\begin{itemize}
  \item un ruban de longueur infinie, divisé en cases ;
  \item une tête capable de lire et d'écrire dans une case et de se déplacer.
\end{itemize}

Les cases du ruban contiennent des symboles faisant partie d'un alphabet $\Sigma$.

Ces aspects physiques étant posés, Turing ajoute plusieurs éléments conceptuels :

\begin{itemize}
  \item un ensemble fini $Q$ représentant les \textit{états}. À chaque étape du calcul, la machine se trouve dans un certain état.
        Trois de ces états sont particuliers : l'état initial $q_0$, l'état d'acceptation $q_a$ et l'état de refus (ou d'arrêt) $q_r$. Ces
        deux derniers états entraînent la fin du calcul ;
  \item une fonction de transition $\delta$ qui à chaque couple (\textit{état de la machine}; \textit{caractère lu}) associe un triplet
        (\textit{caractère écrit}; \textit{déplacement} ; \textit{nouvel état}).
        Le caractère écrit peut être identique à celui lu (pas de modification). Les déplacements décalent, ou non,
        la tête de lecture. S'il y a déplacement, celui-ci se limite à une case vers la gauche ou la droite.
\end{itemize}

La donnée des états et de la fonction de transition définit ce que l'on nomme communément le \textit{programme}.

Initialement la machine se trouve dans un certain état, un certain \textit{mot} est écrit sur le ruban et la tête est positionnée en face d'une
certaine case. La machine se met alors à fonctionner : le caractère lu et l'état actuel permettent de déterminer le caractère à écrire,
le déplacement à effectuer et le prochain état. Ces actions étant effectuées, le processus recommence (si l'on a pas atteint un des deux états d'arrêt).

% Author: Ludger Humbert <https://haspe.homeip.net/cgi-bin/pyblosxom.cgi>
% Source: 
% https://haspe.homeip.net/projekte/ddi/browser/tex/pgf2/turingmaschine-schema.tex
%

\begin{figure}[H]
  \begin{center}
    \begin{tikzpicture}[
        start chain=1 going right,start chain=2 going below,node distance=-0mm
      ]
      \node [on chain=1] {Ruban};
      \node [on chain=1] {};
      \node [on chain=1] {\ldots};
      % \foreach \x in {1,2,...,20} {
      \foreach \x in {B,B,1,0,0,1,0,1,1,0,B,B,B,B,B,B,B} {
          \x, \node [draw,on chain=1, minimum size=7mm] {\x};
        }
      \node [name=r,on chain=1] {\ldots};
      \node [name=k, arrow box, draw,on chain=2, fill=gray,
        arrow box arrows={east:.4cm, west:0.4cm}] at (3.4,-0.7) {};
      \node [right=of k] {Tête de lecture/écriture};
      \node [on chain=2] {};
      \node [draw,on chain=2, fill=lightgray!40] {Programme};
      \chainin (k) [join];
    \end{tikzpicture}
    \caption{Machine de Turing}
  \end{center}
\end{figure}

Considérons par exemple la machine suivante :

\begin{itemize}
  \item l'alphabet est composé des caractères $\{0, 1, B\}$,
  \item la machine comporte trois états $q_0$, $q_1$ et $q_2$,
  \item la machine est initialement dans l'état $q_0$,
  \item l'état d'acceptation est $q_2$,
  \item la fonction de transition est définie dans le tableau ci-dessous.
\end{itemize}

\begin{table}[H]
  \begin{center}
    \begin{tabular}{lllll}
      \multicolumn{2}{l}{\textbf{Entrée}} & \multicolumn{3}{l}{\textbf{Sortie}}                                                \\
      \toprule
      état                                & caractère lu                        & caractère écrit & déplacement & nouvel état  \\
      \midrule
      $q_0$                               & $0$                                 & $0$             & droite      & $q_0$        \\
      $q_0$                               & $1$                                 & $1$             & droite      & $q_0$        \\
      $q_0$                               & $B$                                 & $B$             & gauche      & $q_1$        \\
      $q_1$                               & $1$                                 & $0$             & gauche      & $q_1$        \\
      $q_1$                               & $0$                                 & $1$             & $\emptyset$ & $q_2$        \\
      $q_1$                               & $B$                                 & $1$             & $\emptyset$ & $q_2$        \\
      $q_2$                               & $0$, $1$ ou $B$                     & \textit{idem}   & $\emptyset$ & \textit{fin} \\
      \bottomrule
    \end{tabular}
  \end{center}
\end{table}

Cette machine permet d'incrémenter un nombre binaire comme illustré sur la figure \ref{incrementation}.

\begin{figure}[ht!]
  \begin{center}
    \begin{tikzpicture}[
        start chain=1 going right,
        start chain=2 going right,
        start chain=3 going right,
        start chain=4 going right,
        start chain=5 going right,
        start chain=6 going right,
        start chain=7 going right,
        start chain=8 going right,
        node distance=0mm
      ]
      \node at (0, 1) {\textbf{état actuel}};
      \node at (7.5, 1) {\textbf{action}};

      \node [on chain=1] at (0,0) {$(q_0, 1)$};
      \node [on chain=1] {};
      \node [on chain=1] {};
      \node [on chain=1] {...};
      \node [draw, on chain=1, minimum size=6mm] {$B$};
      \node [draw, on chain=1, minimum size=6mm, fill=lightgray] {1};
      \node [draw, on chain=1, minimum size=6mm] {0};
      \node [draw, on chain=1, minimum size=6mm] {0};
      \node [draw, on chain=1, minimum size=6mm] {1};
      \node [draw, on chain=1, minimum size=6mm] {B};
      \node [on chain=1] {...};
      \node [on chain=1] {};
      \node [on chain=1] {};
      \node [on chain=1] {$(1,q_0, \rightarrow)$};

      \node [on chain=2] at (0, -1) {$(q_0,0)$};
      \node [on chain=2] {};
      \node [on chain=2] {};
      \node [on chain=2] {...};
      \node [draw, on chain=2, minimum size=6mm] {$B$};
      \node [draw, on chain=2, minimum size=6mm] {1};
      \node [draw, on chain=2, minimum size=6mm, fill=lightgray] {0};
      \node [draw, on chain=2, minimum size=6mm] {0};
      \node [draw, on chain=2, minimum size=6mm] {1};
      \node [draw, on chain=2, minimum size=6mm] {B};
      \node [on chain=2] {...};
      \node [on chain=2] {};
      \node [on chain=2] {};
      \node [on chain=2] {$(0,q_0, \rightarrow)$};

      \node [on chain=3] at (0,-2) {$(q_0, 0)$};
      \node [on chain=3] {};
      \node [on chain=3] {};
      \node [on chain=3] {...};
      \node [draw, on chain=3, minimum size=6mm] {$B$};
      \node [draw, on chain=3, minimum size=6mm] {1};
      \node [draw, on chain=3, minimum size=6mm] {0};
      \node [draw, on chain=3, minimum size=6mm, fill=lightgray] {0};
      \node [draw, on chain=3, minimum size=6mm] {1};
      \node [draw, on chain=3, minimum size=6mm] {B};
      \node [on chain=3] {...};
      \node [on chain=3] {};
      \node [on chain=3] {};
      \node [on chain=3] {$(0,q_0, \rightarrow)$};

      \node [on chain=4] at (0, -3) {$(q_0, 1)$};
      \node [on chain=4] {};
      \node [on chain=4] {};
      \node [on chain=4] {...};
      \node [draw, on chain=4, minimum size=6mm] {$B$};
      \node [draw, on chain=4, minimum size=6mm] {1};
      \node [draw, on chain=4, minimum size=6mm] {0};
      \node [draw, on chain=4, minimum size=6mm] {0};
      \node [draw, on chain=4, minimum size=6mm, fill=lightgray] {1};
      \node [draw, on chain=4, minimum size=6mm] {B};
      \node [on chain=4] {...};
      \node [on chain=4] {};
      \node [on chain=4] {};
      \node [on chain=4] {$(1,q_0, \rightarrow)$};

      \node [on chain=5] at (0, -4) {$(q_0, B)$};
      \node [on chain=5] {};
      \node [on chain=5] {};
      \node [on chain=5] {...};
      \node [draw, on chain=5, minimum size=6mm] {$B$};
      \node [draw, on chain=5, minimum size=6mm] {1};
      \node [draw, on chain=5, minimum size=6mm] {0};
      \node [draw, on chain=5, minimum size=6mm] {0};
      \node [draw, on chain=5, minimum size=6mm] {1};
      \node [draw, on chain=5, minimum size=6mm, fill=lightgray] {B};
      \node [on chain=5] {...};
      \node [on chain=5] {};
      \node [on chain=5] {};
      \node [on chain=5] {$(B,q_1, \leftarrow)$};

      \node [on chain=6] at (0, -5) {$(q_1, 1)$};
      \node [on chain=6] {};
      \node [on chain=6] {};
      \node [on chain=6] {...};
      \node [draw, on chain=6, minimum size=6mm] {$B$};
      \node [draw, on chain=6, minimum size=6mm] {1};
      \node [draw, on chain=6, minimum size=6mm] {0};
      \node [draw, on chain=6, minimum size=6mm] {0};
      \node [draw, on chain=6, minimum size=6mm, fill=lightgray] {1};
      \node [draw, on chain=6, minimum size=6mm] {B};
      \node [on chain=6] {...};
      \node [on chain=6] {};
      \node [on chain=6] {};
      \node [on chain=6] {$(0,q_1, \leftarrow)$};

      \node [on chain=7] at (0, -6) {$(q_1, 0)$};
      \node [on chain=7] {};
      \node [on chain=7] {};
      \node [on chain=7] {...};
      \node [draw, on chain=7, minimum size=6mm] {$B$};
      \node [draw, on chain=7, minimum size=6mm] {1};
      \node [draw, on chain=7, minimum size=6mm] {0};
      \node [draw, on chain=7, minimum size=6mm, fill=lightgray] {0};
      \node [draw, on chain=7, minimum size=6mm] {0};
      \node [draw, on chain=7, minimum size=6mm] {B};
      \node [on chain=7] {...};
      \node [on chain=7] {};
      \node [on chain=7] {};
      \node [on chain=7] {$(1,q_2, \emptyset)$};

      \node [on chain=8] at (0, -7) {$(q_2, 1)$};
      \node [on chain=8] {};
      \node [on chain=8] {};
      \node [on chain=8] {...};
      \node [draw, on chain=8, minimum size=6mm] {$B$};
      \node [draw, on chain=8, minimum size=6mm] {1};
      \node [draw, on chain=8, minimum size=6mm] {0};
      \node [draw, on chain=8, minimum size=6mm, fill=lightgray] {1};
      \node [draw, on chain=8, minimum size=6mm] {0};
      \node [draw, on chain=8, minimum size=6mm] {B};
      \node [on chain=8] {...};
      \node [on chain=8] {};
      \node [on chain=8] {};
      \node [on chain=8] {$fin$};

    \end{tikzpicture}
    \caption{Déroulé de l'incrémentation de 1001}
    \label{incrementation}
  \end{center}
\end{figure}

\subsection{Variations}

Bien que très simple, le modèle décrit ci-dessus est extrêmement puissant et permet de modéliser des machines plus élaborées.

Tout d'abord, tout alphabet $\Sigma$ peut être réduit à un alphabet de trois symboles, par exemple $\{0, 1, B\}$
où $B$ est le caractère \textit{blanc}, la case vide.
On peut s'en convaincre en faisant une analogie avec la table ASCII qui  représente chaque caractère par une succession de 8 bits.

On peut aussi imaginer des machines de Turing possédant plusieurs rubans. Plus pratiques pour visualiser certains problèmes (voir ci-dessous),
celles-ci peuvent néanmoins être modélisées par des machines à une seule ruban. Pour ce faire, il faut :
\begin{itemize}
  \item recopier le contenu des différents rubans sur un seul.
        On séparera le contenu de chaque ruban par un caractère spécial (par exemple $\#$),
  \item garder trace de la position de la tête de lecture de chaque ruban en ajoutant à la fin de chaque caractère
        recopié un bit $0$ ou $1$ : un caractère suivi de $1$ correspond à la position de la tête de lecture.
\end{itemize}

Les machines décrites jusqu'à maintenant sont \textit{déterministes} : la connaissance de l'état actuel et de la valeur lue
permet de déterminer de façon certaine les actions à effectuer.
Il est possible d'envisager des machines de Turing non déterministes dans lesquelles la fonction de transition associe plusieurs actions
au même couple (\textit{état de la machine};\textit{caractère lu}).
Là encore, ces machines peuvent être modélisées par une machine de Turing classique.

Nous venons de citer certaines machines de Turing particulières avant de constater, pour chacune, qu'elles étaient modélisables par une
machine de Turing simple (machine déterministe possédant un alphabet limité à trois caractères et un seul ruban).
On peut aussi imaginer d'autres types de machines, d'autres modèles de calculs. Par exemple :
\begin{itemize}
  \item les machines à deux piles, ou plus, dans lesquelles les seules actions possibles sont l'empilement ou le dépilement
        d'un symbole au sommet d'une pile et la lecture/comparaison de la valeur au sommet d'une pile ;
  \item les machines à compteurs, encore plus rudimentaires, qui contiennent un nombre fini de compteurs, tous initialement nuls
        sauf celui codant l'entrée du calcul. Les seules opérations possibles sont le test d'égalité entre deux compteurs,
        l'incrément et le décrément de la valeur d'un compteur.
\end{itemize}

Là encore, ces deux types de machines peuvent aussi être modélisés par des machines de Turing.

\subsection{Lien avec nos ordinateurs}

On le dit souvent, les machines de Turing sont les modèles de nos ordinateurs. Pourquoi ?
Car il est possible de modéliser un ordinateur sur une machine de Turing.

Considérons le modèle des \textit{machines RAM (Random Access Machine)}, modèle très proche des processeurs contemporains.

Une machine RAM est organisée autour de registres (en nombre infini) désignés par une adresse (un entier) et
capables de stocker des entiers (sans limite de taille).

Une machine RAM peut aussi effectuer certaines actions :
\begin{itemize}
  \item copier une valeur d'un registre à l'autre,
  \item récupérer la valeur d'un registre dont l'adresse est donnée par un autre,
  \item écrire dans un registre dont l'adresse est donnée par un autre,
  \item effectuer des calculs sur la valeur d'un registre (additionner 1, le comparer à 0...),
  \item effectuer des opérations sur les valeurs de deux registres (les additionner, les soustraire...).
\end{itemize}

On notera que, mis à part l'aspect infini (taille des entiers stockés et nombre de registres), cette description
correspond bien au fonctionnement de nos processeurs.

Une telle machine peut être simulée par une machine de Turing. On peut par exemple utiliser une machine à quatre rubans,
le premier stockant les adresses des registres, adresses séparées par un caractère blanc, le deuxième contenant
les valeurs des registres, dans le même ordre que les adresses et aussi séparées par un caractère blanc, le troisième
servant d'accumulateur et le dernier d'espace de travail.

Les actions, les \textit{calculs}, réalisables par un ordinateur peuvent donc être réalisés par une machine de Turing.

\subsection{Machine de Turing universelle}

Une machine de Turing est associée à un ensemble d'états et une fonction de transition (le \textit{programme} de la machine).
Il n'existe donc pas \textbf{une} mais \textbf{des} machines de Turing, une par programme.


Le programme d'une machine de Turing peut être représenté sous forme d'un nombre binaire, il suffit de choisir un code
permettant de représenter les différents états ainsi que la fonction de transition $\delta : (\text{état}, \text{valeur}) \mapsto (\text{valeur écrite}, \text{déplacement}, \text{prochain état})$.
On peut par exemple imaginer le codage associant à chaque état, chaque caractère de l'alphabet et chaque déplacement possible, un nombre.
La machine est alors représentée par la succession de ces nombres, écrits en unaire (par exemple 5=11111), tous séparés par des 0 ou des
00 (les simples 0 séparent des objets de mêmes types, deux états par exemple, et les doubles 00 séparent les types différents, les états et les
caractères sont séparés par 00).

Dès lors le code d'une machine peut être écrit sur une autre machine. Celle-ci peut le décoder facilement afin de savoir quelles
sont les instructions à effectuer. Une telle machine peut fonctionner avec trois rubans : le premier contient le code de la
machine à simuler, le deuxième le mot d'entrée du calcul et le dernier sert d'espace de travail.

On construit ainsi une machine de Turing \textit{universelle}, machine programmable capable de modéliser \textbf{toutes} les machines de Turing.
Une telle machine prend en entrée un couple $(\langle C,\omega\rangle)$ dans lequel $\langle C \rangle$ est le code représentant
la machine $C$ à modéliser et $\omega$ l'entrée du calcul.

Un programme informatique peut donc être considéré comme une entrée d'une machine de Turing au même titre qu'un nombre quelconque. De la même
façon, les programmes s'exécutant sur nos machines sont des données d'un \textit{meta}-programme : le système d'exploitation.

\section{Calculabilité}

On a présenté ci-dessus différents modèles de calculs pouvant tous être modélisés par une machine de Turing. Il est possible de montrer
que ces modèles sont équivalents : ce qui est calculable avec l'un est calculable avec les autres.

La thèse de \textit{Church-Turing} va plus loin et affirme que \og ce qui est effectivement calculable (par un être humain en un temps fini)
est calculable par une machine de Turing \fg.

Cette thèse n'est pas un théorème : dans la mesure la définition de \og \textit{ce qui est effectivement calculable} \fg\
n'est pas une définition formelle, il n'est pas question de prouver une thèse s'appuyant sur des bases peu claires.

La seconde mention du terme \og\textit{calculable} \fg\ est toutefois plus précise : est \og \textit{calculable} \fg\ par une
machine de Turing, un nombre qu'une machine de Turing est capable d'écrire sur son ruban en un nombre fini d'étapes avant d'arriver
dans l'état d'acceptation.

Comme on a pu s'en rendre compte, la programmation à l'aide de machines de Turing est rapidement fastidieuse.
Il est néanmoins possible d'utiliser des machines de Turing afin de modéliser le fonctionnement des ordinateurs et langages de programmation
contemporains.
De tels langages, permettant de réaliser les mêmes calculs qu'une machine de Turing, sont dit \textit{Turing-complet}.
\texttt{Python}, \texttt{Java}, \texttt{C} mais aussi le couple \texttt{HTML/CSS} ainsi que \LaTeX\ sont Turing-complets !

\subsection{Problèmes de décision}

Un problème de décision est un problème dont la réponse est \og \textit{Vrai} \fg\ ou \og \textit{Faux} \fg.

Formellement, il s'agit d'un ensemble $E$, les instances du problème, et d'un sous-ensemble $E^+\subset E$ contenant les \og \textit{instances positives de $E$} \fg.
Résoudre le problème pour une instance $\omega \in E$ c'est déterminer si $\omega$ est aussi un élément de $E^+$.

On peut par exemple considérer le problème dont les instances sont les entiers naturels $E=\mathbb{N}$ et les instances positives les nombres premiers $E^+=\mathbb{P}$. Ce problème
peut-être formulé ainsi : \og \textit{Soit n un entier naturel. n est-il premier ?}.



\subsection{Problème décidable}

Soit $P$ un problème de décision. $P$ est dit \textit{décidable} s'il est possible de répondre par \og \textit{Vrai} \fg\
ou \og \textit{Faux} \fg\ à toutes les instances $\omega$ de $P$.

Le problème \og \textit{$n$ est-il un nombre premier ?} \fg\ est décidable.


\begin{figure}[H]
  \begin{center}
    \begin{tikzpicture}
      \draw[draw=black, fill=lightgray!20] (-0.5,0.5) rectangle ++(2.6,-2.7);
      \node (zero) at (0, 0) {$0$};
      \node[right=0.3 cm of zero] (un) {$1$};
      \node[right=0.3 cm of un] (deux) {$2$};
      \node[below=0.3cm of zero] (trois) {$3$};
      \node[right=0.3cm of trois] (quatre) {$4$};
      \node[right=0.3cm of quatre] (cinq) {$5$};
      \node[below=0.3cm of trois] (six) {$6$};
      \node[right=0.3cm of six] (sept) {$7$};
      \node[right=0.3cm of sept] (etc) {$...$};
      \node[below=0.5 of sept] (N) {$E=\mathbb{N}$};
      
      \draw[draw=black, fill=lightgray!20] (-0.5,0.5)++(4.6,0) rectangle ++(2.6,-2.7);
      \node[left=2 of un] (unbis) {$\times$};
      \node[right=4 of deux] (deuxbis) {$2$};
      \node[right=4 of trois] {$3$};
      \node[right=4 of cinq] (cinqbis) {$5$};
      \node[right=4 of sept] (septbis) {$7$};
      \node[right=4 of etc] {$...$};
      \node[below=0.5 of septbis] {$E^+=\mathbb{P}$};

      \draw[-latex] (un) to[out=160,in=20] (unbis);
      \draw[-latex] (cinq) to[out=20,in=160] (cinqbis);
    \end{tikzpicture}
    \caption{$n$ est-il premier ?}
  \end{center}
\end{figure}


Il est ici nécessaire d'introduire brièvement la notion de \textit{langage}. Un langage est la donnée d'un \textit{alphabet} $\Sigma$
et d'un ensemble de \textit{mots} $\Sigma^*$ s'obtenant à l'aide de certaines règles (la concaténation de chaînes par exemple).
Les mots peuvent être en nombre limité ou non. Il est possible dans préciser et restreindre l'ensemble des mots valides en en donnant
une définition récursive à partir de mots de base.

Par exemple, sur l'alphabet $\{(,)\}$, en considérant le mot de base $\epsilon$ (mot vide), on peut considérer les deux règles ci-dessous :
\begin{itemize}
  \item si $\omega$ est un mot alors $(\omega)$ est un mot,
  \item si $\omega_1$ et $\omega_2$ sont des mots alors $\omega_1.\omega_2$ sont des mots. (l'opération $.$ est la concaténation de deux mots).
\end{itemize}

Ces règles permettent de définir le langage de Dyck (les expressions bien parenthésées, \og (())() \fg est un mot de Dyck, pas \og ()) \fg).

Un problème $P$ est décrit à l'aide d'une certaine fonction de codage (les mots de Dyck peuvent par exemple être décrits en précisant
la taille du mot et les indices des parenthèses ouvrantes). Ce code s'exprime dans un certain alphabet $\Sigma$.
Le langage associé à $P$ est alors l'ensemble des mots $L(P)$ représentants les instances de $E^+$ (les instances positives de $E$).

On peut aussi construire la transformation inverse en associant à chaque langage un problème de décision : étant donné
un mot quelconque $\omega$, fait-il partie du langage ?

À l'instar des problèmes, un langage peut donc être décidable. Un langage est décidable s'il est associé à
un problème décidable par une machine de Turing.

Le langage des mots de Dyck est décidable et associé au problème \og \textit{Soit $\omega$ une
  suite de parenthèses. $\omega$ est-il un mot de Dyck ?} \fg.

\subsection{Problèmes indécidables}

On peut donc mettre en correspondance problèmes et langages.

Une machine de Turing peut être codée par un nombre fini (un mot composé de $0$ et de $1$ lu en binaire tel que décrit dans la partie
sur les machines universelles). L'ensemble des machines de Turing est donc dénombrable (de cardinal inférieur ou égal à celui de $\mathbb{N}$).

L'ensemble des langages peut, quant à lui, être mis en correspondance avec l'ensemble des parties de $\mathbb{N}$ (un langage contient des mots de
1 caractère, 2 caractères, ...).

Or le théorème de Cantor affirme que la cardinal de l'ensemble des parties d'un ensemble $E$ est strictement supérieur à celui de $E$.

Pour le prouver supposons qu'il existe une application $f$ faisant correspondre à chaque élément de $E$ une partie de $E$ (application surjective
de $E$ dans l'ensemble des parties de $E$).
Considérons la partie $D=\{x \in E, x \notin f(x)\}$ c'est à dire l'ensemble des éléments de
$E$ qui ne font pas partie de leur image par $f$. Montrons par l'absurde que cet ensemble n'est l'image d'aucun $y$ de $E$.

On suppose donc qu'il existe $y$ tel que $f(y)=D$. Deux cas se présentent à nous :
\begin{itemize}
  \item $y \in D$ : c'est impossible car $D$ est l'ensemble des éléments qui ne sont pas contenus dans leur image par $f$ ;
  \item $y \notin D$ donc $y$ est tel qu'il doit faire partie de son image par $f$ ($D$ est l'ensemble des éléments de faisant \textbf{pas} partie de leur image). On aboutit là encore à une contradiction.
\end{itemize}

On en conclut donc que $D$ n'a pas d'antécédent par $f$. Donc $f$ n'est par surjective et l'ensemble des parties de $E$ est plus \og grand \fg\ que $E$.

Appliqué à nos problèmes et langages, cela signifie donc que l'ensemble des problèmes, des langages, est de cardinal
strictement supérieur à celui des machines de Turing. Donc il existe de plus de problèmes que de machines pouvant les résoudre : \textbf{certains problèmes sont donc indécidables par une machine de Turing}.

\subsection{Problème de la décision}

Est-il possible de déterminer si une machine de Turing $M$ va accepter un mot quelconque $\omega$ ? I s'agit du problème de la décision,
le \textit{Entscheidungsproblem} cité dans l'article de Turing.

Supposons q'il existe une machine $A$ effectuant cette tâche : $A$ prend en argument le code $\langle M \rangle$ de $M$ et le mot $\omega$
et accepte ce couple en entrée si $M$ accepte le mot $\omega$, le refuse si $M$ refuse $\omega$.

Construisons une machine $B$ prenant comme entrée le code $\langle C\rangle$ d'une machine $C$.
$B$ fournit le couple $\langle \langle C \rangle, \langle C \rangle \rangle$ à la machine $A$
et accepte l'entrée si $A$ refuse le couple, refuse l'entrée si $A$ l'accepte.

$A$ répondant au problème de décision posé, il est clair que $B$ se termine, positivement ou négativement, sur toute entrée.

Fournissons pour finir l'entrée $\langle B \rangle$ à $B$ (son propre code).
Si $A$ accepte le couple $\langle \langle B \rangle, \langle B \rangle \rangle$ correspondant, cela signifie que $B$ accepte l'entrée.
Or lorsque $A$ accepte l'entrée, par construction, $B$ la refuse. On est face à une contradiction.

Inversement, si $A$ refuse l'entrée alors $B$ est censée l'accepter. Mais le fait que $A$ refuse indique que $B$ refuse...
De nouveau une contradiction.

On peut donc conclure qu'il n'existe pas de machine de Turing permettant de juger si une autre machine accepte ou non un mot quelconque.
Cela ferme complètement la porte à l'existence d'un \textit{debugger} parfait... Mathématiquement parlant cela signifie aussi qu'il n'existe aucune
machine de calcul, aucun programme informatique, permettant de vérifier qu'une proposition est un théorème valide ou si elle est fausse.

\subsection{Problème de l'arrêt}

Restreignons le problème : est-il possible, à défaut de dire si une machine quelconque accepte ou non un mot, de déterminer si elle s'arrête ?
Il s'agit du \textit{halting problem}, ou \textit{problème de l'arrêt} en français.

La preuve est proche de la précédente. Supposons qu'il existe une machine $A$ permettant de décider ce problème.
On construit à nouveau une machine $B$ à partir de $A$ :
\begin{itemize}
  \item lorsque $B$ prend une entrée $\langle C\rangle$, elle transmet l'entrée $(\langle C\rangle, \langle C\rangle)$ à $A$,
  \item si $A$ refuse cette entrée alors $B$ accepte.
  \item si $A$ accepte cette entrée, $B$ boucle infiniment,
\end{itemize}

\begin{figure}[H]
  \begin{center}
    \begin{tikzpicture}
      \node (omega) at (0, 0) {$\omega$};
      \node[draw, right=1.2cm of omega, minimum size=1cm, fill=gray!30] (A) {$A$};
      \draw[draw=black] (1,-1.5) rectangle ++(6,3);
      \node[right=2cm of A] (Abis) {};
      \node[above=0.5cm of Abis,] (r) {$refuse$};
      \node[below=0.5cm of Abis] (a){$accepte$};
      \node[right=2.5cm of r] (aa) {$accepte$};
      \node[right=0.5cm of a] (rr) {};
      \draw [-latex] (omega.east) --  (A.west) ;
      \draw [-latex] (A.east) --  (r.west) ;
      \draw [-latex] (A.east) --  (a.west) ;
      \draw [-latex] (r.east) --  (aa.west) ;
      \draw [-latex] (a.east) --  (rr.west) ;
      \draw[->] (rr.east)++(0,0.1) arc (160:-160:.35);
      % \draw[-latex,out=0,in=180] (rr.north east) to (rr.south west);
    \end{tikzpicture}
    \caption{La machine $B$ (problème de l'arrêt)}
  \end{center}
\end{figure}

Là encore proposons l'entrée $\langle B\rangle$ à $B$. $A$ reçoit donc l'entrée $(\langle B\rangle, \langle B\rangle)$.
Deux cas se présentent :
\begin{itemize}
  \item $A$ accepte cette entrée.
        Cela signifie donc que $B$ est supposée se terminer sur l'entrée $\langle B\rangle$.
        Mais dans ce cas, par construction $B$ boucle infiniment et ne se termine pas : contradiction ;
  \item $A$ refuse l'entrée, indiquant donc que $B$ ne se termine pas sur cette entrée.
        Mais par construction, si $A$ refuse l'entrée, $B$ termine en l'acceptant : nouvelle contradiction.
\end{itemize}

On prouve ainsi que le problème de l'arrêt est indécidable : il n'existe aucun programme capable de
juger si un autre programme s'arrêtera ou non sur une entrée quelconque.

\end{document}