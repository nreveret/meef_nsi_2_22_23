def somme_2(tableau, i=0, partielle=0):
    if i == len(tableau):
        return partielle
    return somme_2(tableau, i + 1, tableau[i] + partielle)

assert somme_2([3, 2, 5]) == 10
