def fibo(n, valeurs=None):
    if valeurs is None:
        valeurs = [None] * (n + 1)
        valeurs[0] = 0
        valeurs[1] = 1

    if valeurs[n] is None:
        valeurs[n] = fibo(n - 2, valeurs) + fibo(n - 1, valeurs)
    return valeurs[n]


print(fibo(6))
