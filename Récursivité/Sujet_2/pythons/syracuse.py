def syracuse(N):
    print(N)
    if N != 1:
        if N % 2 == 0:
            syracuse(N // 2)
        else:
            syracuse(3 * N + 1)


syracuse(7)
