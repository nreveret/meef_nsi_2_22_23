def compte_a_rebours(n):
    if n == 0:
        print("Partez !")
    else:
        print(n)
        compte_a_rebours(n - 1)


def n_entiers(n):
    if n == 1:
        print(n - 1)
    else:
        n_entiers(n - 1)
        print(n - 1)


def somme(tableau):
    if len(tableau) == 1:
        return tableau[0]
    else:
        return tableau[0] + somme(tableau[1:])


def somme_chiffres(n):
    if n < 10:
        return n
    else:
        return n % 10 + somme_chiffres(n // 10)


def listes_imbriquees(n):
    return [] if n == 1 else [listes_imbriquees(n - 1)]


def profondeur(liste):
    if liste == []:
        return 1
    else:
        return 1 + profondeur(liste[0])


def somme_bis(tableau, i=0):
    if i == len(tableau):
        return 0
    else:
        return tableau[i] + somme_bis(tableau, i + 1)


def somme_2(tableau, i=0, partielle=0):
    if i == len(tableau):
        return partielle
    return somme_2(tableau, i + 1, tableau[i] + partielle)


def fibo_rec(n):
    if n <= 1:
        return n
    else:
        return fibo_rec(n - 1) + fibo_rec(n - 2)


fibo_rec(5)


def fibo(n, valeurs=None):
    # if valeurs is None:
    #     valeurs = [None] * (n + 1)
    #     valeurs[0] = 0
    #     valeurs[1] = 1

    if valeurs[n] is None:
        valeurs[n] = fibo(n - 2, valeurs) + fibo(n - 1, valeurs)
    return valeurs[n]


print(fibo(10))

[0, 1, 1, 2, 3, 5, 8, 13, 21, 34]
