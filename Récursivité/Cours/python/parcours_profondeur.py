def parcours_profondeur(sommet, graphe, sommets_visites=None):
    if sommets_visites is None:
        sommets_visites = []

    sommets_visites.append(sommet)
    for voisin in graphe[sommet]:
        if voisin not in sommets_visites:
            parcours_profondeur(voisin, graphe, sommets_visites)
    return sommets_visites


graphe = {
    "A": ["C"],
    "B": ["G"],
    "C": ["A", "D", "G"],
    "D": ["C", "E"],
    "E": ["D"],
    "F": ["G"],
    "G": ["B", "F"],
}

print(parcours_profondeur("A", graphe))
