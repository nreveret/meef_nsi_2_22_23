def fibo_rec(n):
    fibo_rec.compteur += 1
    if n <= 1:
        return n
    else:
        return fibo_rec(n - 1) + fibo_rec(n - 2)


def fibo_memo(n, F=None):
    fibo_memo.compteur += 1

    if F is None:
        F = [None] * (n + 1)
        F[0] = 0
        F[1] = 1

    if F[n] is None:
        F[n] = fibo_memo(n - 1, F) + fibo_memo(n - 2, F)
    return F[n]


def fibo_iter(n):
    F = [0, 1]

    while len(F) <= n:
        F.append(F[-1] + F[-2])

    return F[n]


fibo_rec.compteur = 0
fibo_rec(10)
print(fibo_rec.compteur)

fibo_memo.compteur = 0
fibo_memo(10)
print(fibo_memo.compteur)

fibo_iter(10)
