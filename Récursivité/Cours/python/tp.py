from random import randint


# Triangle décroissant
def triangle_decroissant(taille):
    """
    triangle_decroissant(4) renvoie :
    ####
    ###
    ##
    #
    """
    pass


for i in range(1, 100):
    assert triangle_decroissant(i) == "\n".join(["#" * k for k in range(i, 0, -1)])


# Triangle croissant
def triangle_croissant(taille):
    """
    triangle_croissant(4) renvoie :
    #
    ##
    ###
    ####
    """
    pass


for i in range(1, 100):
    assert triangle_croissant(i) == "\n".join(["#" * k for k in range(1, i + 1)])


# Maximum d'une liste
def maximum(tableau):
    "Déterminer la valeur maximale dans un tableau"
    pass


for _ in range(10):
    liste = [randint(1, 100) for _ in range(200)]
    assert maximum(liste) == max(liste)


# Somme d'une liste
def somme(tableau):
    "Calculer la somme des valeurs d'un tableau"
    pass


for _ in range(10):
    liste = [randint(1, 100) for _ in range(200)]
    assert somme(liste) == sum(liste)


# Convertir un nombre en str vers le int correpondant
def entier(chaine):
    "Convertit un nombre en chaîne de caractères en entier"
    pass


for _ in range(10):
    nombre = randint(0, 10**6)
    assert entier(str(nombre)) == nombre


# Convertir un nombre en str vers le int correpondant. Avec la méthode de Horner
def entier_horner(chaine):
    "Convertit un nombre en chaîne de caractères en entier. Avec la méthode de Horner"
    pass


for _ in range(10):
    nombre = randint(0, 10**6)
    assert entier_horner(str(nombre)) == nombre


# Maximum d'une liste SANS TRANCHES
def maximum_sans_tranches(tableau, i=0):
    "Déterminer la valeur maximale dans un tableau. On interdit les tranches ou copies du tableau"
    pass


for _ in range(10):
    liste = [randint(1, 100) for _ in range(200)]
    assert maximum_sans_tranches(liste) == max(liste)


# Somme d'une liste SANS TRANCHES
def somme_sans_tranches(tableau, i=0):
    "Calculer la somme des valeurs d'un tableau. On interdit les tranches ou copies du tableau"
    pass


for _ in range(10):
    liste = [randint(1, 100) for _ in range(200)]
    assert somme_sans_tranches(liste) == sum(liste)


# Convertir un nombre en str vers le int correpondant. Avec la méthode de Horner et sans tranches
def entier_horner_sans_tranches(chaine, i=None):
    "Convertir un nombre en str vers le int correpondant. Avec la méthode de Horner et sans tranches"
    pass


for _ in range(10):
    nombre = randint(0, 10**6)
    assert entier_horner_sans_tranches(str(nombre)) == nombre
