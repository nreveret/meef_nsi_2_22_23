from random import choice
from time import perf_counter


def graphe_aleatoire(nb_sommets, nb_aretes):
    sommets = [str(k) for k in range(nb_sommets)]
    graphe = {s: set() for s in sommets}
    for arete in range(nb_aretes):
        s1 = choice(sommets)
        s2 = s1
        while s2 == s1 or s2 in graphe[s1]:
            s2 = choice(sommets)
        graphe[s1].add(s2)
        graphe[s2].add(s1)

    return graphe


def parcours_profondeur(sommet, graphe, sommets_visites=None, profondeur=1):
    parcours_profondeur.appels += 1
    parcours_profondeur.prof_max = max(parcours_profondeur.prof_max, profondeur)

    if sommets_visites is None:
        sommets_visites = []

    if sommet not in sommets_visites:
        sommets_visites.append(sommet)
        for voisin in graphe[sommet]:
            if voisin not in sommets_visites:
                parcours_profondeur(voisin, graphe, sommets_visites, profondeur + 1)
    return sommets_visites


def parcours_profondeur_iter(sommet, graphe):
    sommets_visites = set()
    ordre = []
    pile = [sommet]
    while pile:
        sommet = pile.pop()
        if sommet not in sommets_visites:
            sommets_visites.add(sommet)
            ordre.append(sommet)
            for voisin in graphe[sommet]:
                if voisin not in sommets_visites:
                    pile.append(voisin)
    return ordre


graphe = graphe_aleatoire(900, 100_000)

parcours_profondeur.appels = 0
parcours_profondeur.prof_max = 0

debut = perf_counter()
a = parcours_profondeur("0", graphe)
fin = perf_counter()
print(f"Parcours récursif : {fin - debut}s")
print(f"Il y a eu {parcours_profondeur.appels} appels")
print(f"La profondeur maximale de la récursivité vaut {parcours_profondeur.prof_max}")

debut = perf_counter()
b = parcours_profondeur_iter("0", graphe)
fin = perf_counter()
print(f"Parcours itératif : {fin - debut}s")
