\documentclass[a4paper, 11pt]{article}

\title{
  \rule{\linewidth}{0.8pt}
  \Large{\scshape{MEEF - NSI}}

  \vspace{0.5cm}
  \large{\textit{Université Catholique de l'Ouest}}

  \vspace{0.5cm}
  \Large{\textbf{Récursivité}}

  \rule[10pt]{\linewidth}{0.8pt}
    }
\author{N. Revéret}
\date{\today}

%-------------------------------------------------------
% Les packages
%-------------------------------------------------------
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
% \usepackage{multicol} % les colonnes
\usepackage[french]{babel}
\addto\captionsfrench{\def\tablename{Tableau}}
\addto\captionsfrench{\def\figurename{Figure}}
\usepackage{lmodern}
\usepackage{hyperref}     % liens hypertextes
\usepackage{graphicx}   % insertion d'images
\usepackage{booktabs}     % Les tableaux
\usepackage{amssymb}      % symboles maths
\usepackage{amsmath}      % symboles maths
% \usepackage{blindtext}  % faux texte
\usepackage{float}        % positionnement des tableaux et autres flottants
\usepackage{multirow}     % lignes fusionnées dans les tableaux 
\usepackage{lastpage}     % compter les pages
\usepackage{soul}         % surlignement du texte
\usepackage{stmaryrd}     % doubles crochets
%-------------------------------------------------------
% Figure geogebra -> tikz
%-------------------------------------------------------
\usepackage{pgfplots}
\pgfplotsset{compat=1.15}
\usepackage{mathrsfs}
\usetikzlibrary{arrows, positioning, snakes, calc, shapes.misc, decorations.shapes, decorations.markings, backgrounds}

%-------------------------------------------------------
% Dimension de la page
%-------------------------------------------------------
\usepackage[a4paper, total={17cm, 25cm}, includehead, headsep=12pt]{geometry} % taille de la page et marges

%-------------------------------------------------------
% Longueurs personnelles
%-------------------------------------------------------
% \setlength{\textfloatsep}{0.1cm}
\setlength{\parskip}{10pt} % 6pt par défaut
\setlength{\parindent}{15pt}
% \setlength{\textfloatsep}{10pt plus 1.0pt minus 2.0pt}
% \setlength{\floatsep}{10pt plus 1.0pt minus 2.0pt}
% \setlength{\intextsep}{10pt plus 1.0pt minus 2.0pt}
% \newlength{\aftercode}
% \setlength{\aftercode}{20pt}

%-------------------------------------------------------
% Les entêtes et pied de pages
%-------------------------------------------------------
\usepackage{fancyhdr}
\pagestyle{fancy}
\fancyhf{}
\fancyhead[L]{\scriptsize MEEF - NSI}
\fancyhead[R]{\scriptsize Récursivité}
\fancyfoot[C]{\scriptsize - $\thepage$/\pageref{LastPage} -}
\renewcommand{\headrulewidth}{0.3pt}
\renewcommand{\footrulewidth}{0.3pt}

%-------------------------------------------------------
% La numérotation des questions et des listes
%-------------------------------------------------------
\usepackage[shortlabels]{enumitem}
\setlist[itemize,1]{label=\textbullet} % le label des listes de niveau 1 est un bullet
\setlist[itemize,2]{label=$\circ$} % le label des listes de niveau 2 est un bullet vide
\setlist[itemize,3]{label=$\diamond$} % le label des listes de niveau 3 est un diamant


%-------------------------------------------------------
% Dossiers contenant les images
%-------------------------------------------------------
\graphicspath{ 
    {images/}
}

%-------------------------------------------------------
% L'environnement Exercice
%-------------------------------------------------------
\newcounter{exercice}
\newenvironment{Exercice}[1][]{
    \refstepcounter{exercice}
    \par
    \noindent
    {\large
    \underline{\textit{Exercice~\theexercice~:}}~#1}
    % \vspace{\parskip}
    \par}
    {\vspace{1em}}

%-------------------------------------------------------
% L'environnement Question
%-------------------------------------------------------
\newcounter{question}[exercice]
\newenvironment{Question}{
    \refstepcounter{question}
    \begin{list}{}{%
      \setlength{\labelindent}{\parindent}
      \setlength{\leftmargin}{2\parindent}
      \setlength{\parsep}{\parskip} % Espacement vertical des sous-paragraphes
    }
    \item[\medskip\textbf{\thequestion.}]}
    {\end{list}}

%-------------------------------------------------------
% L'environnement subQuestion
%-------------------------------------------------------
\newcounter{subquestion}[question]
\newenvironment{subQuestion}{
    \refstepcounter{subquestion}
    \begin{list}{}{%
      \setlength{\leftmargin}{12pt}
      \setlength{\labelwidth}{8pt}
      \setlength{\labelsep}{4pt}
      \setlength{\listparindent}{0em}
      \setlength{\itemindent}{0em}
      \setlength{\parsep}{\parskip} % Espacement vertical des sous-paragraphes
    }
    \item[\medskip\textbf{\alph{subquestion}.}]}
    {\end{list}}
  

%-------------------------------------------------------
% Mise en forme du code
%-------------------------------------------------------
\usepackage[newfloat=false]{minted}
\setminted{autogobble, tabsize=4, breaklines, breakafter=_, linenos, numberblanklines=false, resetmargins, frame=leftline}
% Environnement python code
\newminted{python}{} % permet d'utiliser \begin{pythoncode} pour écrire du python

% Environnement mintinlinepy
\newmintinline[mintinlinepy]{python}{breaklines, breakafter=_} % permet d'utiliser \py{...} pour écrire du python en ligne
\newcommand{\py}{\mintinlinepy} % on peut utilise \py{code...}
    
% Environnement textcode
\newminted{text}{} % permet d'utiliser \begin{textcode} pour écrire du code en français
% \BeforeBeginEnvironment{textcode}{\vspace{-2\parskip}\begin{listing}[H]}
% \AfterEndEnvironment{textcode}{\end{listing}\vspace{-\aftercode}}

% Environnement textcode
\newmintinline[mintinlinetext]{text}{breaklines, breakafter=_} % permet d'utiliser \mintinlinetext{...} pour écrire du code en ligne en français


% Environnement de code sur plusieurs pages
\usepackage{caption}
\newenvironment{longcode}{\captionsetup{type=listing}}{}

%-------------------------------------------------------
% Commandes personnelles
%-------------------------------------------------------
\newcommand{\python}{\texttt{Python}}
\renewcommand{\thesection}{\Alph{section}.}
\renewcommand{\thesubsection}{\Alph{section}.\arabic{subsection}.}
%-------------------------------------------------------

% Le document
%-------------------------------------------------------
\begin{document}


%-------------------------------------------------------
% La page de titre
%-------------------------------------------------------
\maketitle

\vfill

%-------------------------------------------------------
% La table des matières
%-------------------------------------------------------
\setcounter{tocdepth}{3}
\tableofcontents
\newpage

La récursivité est une notion à l'intersection des mathématiques et de l'informatique.
Très efficace en terme de concision du code, c'est aussi une méthode très simple pour rendre un code défectueux.

\section{Récursivité}

\subsection{Définition}

Une fonction récursive est une fonction qui lors de son exécution s'appelle elle-même ou appelle une autre fonction qui l'appellera 
(\py{f} appelle \py{g} qui appelle \py{f}).

\begin{figure}[h]
  \centering
  \includegraphics[width=0.4\textwidth]{recursion}
  \caption{Récursivité}
\end{figure}

L'exemple archi-classique est celui de la fonction calculant la \textit{factorielle} d'un nombre. On rappelle que la factorielle d'un entier naturel $n \ge 0$ vaut $1$ si $n$ vaut $0$ (par convention) ou :

$$factorielle(n) = n\,! = n \times (n-1) \times \dots \times 1$$

Un code récursif possible pour une telle fonction est :

\begin{minted}{python}
def facto(n):
  if n <= 1 :   # Cas de base :
    return 1
  else :        # Cas général
    return n * facto(n - 1)
\end{minted}

Comme on peut le voir, le code est structuré autour de deux cas :

\begin{itemize}

  \item le \textbf{cas de base} : permet de sortir de la récursivité, sans lui, on rentrerait dans une boucle infinie (ou en tout cas une boucle qui ferait « \textit{bugger} » python)
  \item le \textbf{cas général} : c'est dans ce bloc que s'effectue l'appel récursif. Notez l'appel de la même fonction avec une valeur décrémentée du paramètre (\py{n - 1})

\end{itemize}

Que se passe-t'il lorsque l'on appelle \py{facto(4)} ? Python exécute le code ainsi :

$$\begin{aligned}
    facto(4) & = 4\times facto(3)                    \\
             & = 4\times 3 \times facto(2)           \\
             & = 4 \times 3 \times 2 \times facto(1) \\
             & = 4 \times3 \times 2 \times 1         \\
             & = 24
  \end{aligned}
$$

Pour bien illustrer ce cas de figure, considérons la fonction suivante :

\begin{minted}{python}
def inc(n):
  if n == 5:
    print(f"On s'arrête à n={n}")
  else:
    print(f"n vaut {n}")
  return inc(n+1)
\end{minted}

Que produit l'appel \py{inc(0)} ?

\begin{minted}{pycon}
n vaut 0
n vaut 1
n vaut 2
n vaut 3
n vaut 4
On s'arrête à n=5
\end{minted}

Que se passe-t'il ? En fait la fonction s'appelle elle-même en incrémentant la valeur du paramètre de $1$ à chaque fois.
Lorsque \py{n} vaut $5$, les appels récursifs se terminent.

Une bonne façon de décrire le fonctionnement de ces appels est un \textit{pile}: lors des appels successifs, le système les empile sur la \textbf{pile d'exécution}. A chaque nouvel empilement, l'exécution d'une fonction est mise en attente jusqu'à ce qu'elle soit de nouveau en haut de la pile.

Lorsqu'une fonction se termine (avec un \py{return} par exemple), elle est dépilée et son résultat est transmis à la fonction qui l'avait appelée, fonction qui était juste en-dessous dans la pile et qui se retrouve désormais au sommet. Cette dernière est alors exécutée....

On peut illustrer cela avec la figure 2.

\begin{figure}[h]
  \centering
  \includegraphics[width=1\textwidth]{pileExecution2}
  \caption{La pile d'exécution de \py{inc(0)}}
\end{figure}

\textit{Remarque :} Que se passe-t'il si l'on appelle \py{inc(6)} ?

\begin{minted}{pycon}
n vaut 6
n vaut 7
n vaut 8
n vaut 9
...
RecursionError: maximum recursion depth exceeded while calling a Python object
\end{minted}

On obtient une erreur. En effet, notre fonction incrémente \py{n} jusqu'à ce qu'il soit égal à $5$.
Or en débutant à $6$, on n'atteindra jamais la valeur $5$ : le système empile donc des fonctions dans la pile d'exécution sans jamais dépiler :
la pile déborde (\href{https://stackoverflow.com/}{\textit{stack overflow !}}). Une sécurité du langage permet de limiter ces cas de figure aux alentours de $1\,000$ appels récursifs (cette valeur dépend de l'OS).



Il est possible de déterminer la profondeur limite de récursivité utilisé par python en utilisant \href{https://docs.python.org/3/library/sys.html#sys.getrecursionlimit}{\py{sys.getrecursionlimit()}}.
On peut la modifier en faisant \href{https://docs.python.org/3/library/sys.html#sys.setrecursionlimit}{\py{sys.setrecursionlimit(nouvelle_limite)}}.

\subsection{Récursivité terminale}

Le code présenté ci-dessus pour le calcul de la factorielle d'un entier n'est pas idéal.
Pour bien s'en rendre compte reprenons la suite de calculs effectués lors de l'appel de \py{facto(4)} :

$$\begin{aligned}
    facto(4) & = 4\times facto(3)                    \\
             & = 4\times 3 \times facto(2)           \\
             & = 4 \times 3 \times 2 \times facto(1) \\
             & = 4 \times3 \times 2 \times 1         \\
             & = 24
  \end{aligned}
$$

Comme on peut le voir, le calcul à effectuer \og gonfle \fg à chaque étape. Imaginez le calcul de \py{facto(1000)}...
Ce point pose problème car il va engendrer une grande occupation de la mémoire pour python :

\begin{itemize}
  \item d'une part lors de l'exécution, la pile d'exécution va se remplir avec l'ensemble des appels (classique lors de la récursivité, on ne peut pas vraiment faire autrement...)
  \item d'autre part, une zone de mémoire va devoir stocker les valeurs précédant chacun des $facto(n)$ dans les calculs lors de chaque appel (stockage de $4$, stockage de $3$, stockage de $2$...)
\end{itemize}

La raison fondamentale de ce problème est que la récursivité n'est pas \textit{terminale} : l'appel récursif n'est pas le dernier à être exécuté par le code.
En effet dans la ligne \py{return n * factorielle(n - 1)}, la dernière opération (en termes de priorités opératoires) est la multiplication.
A cause de cela, python a besoin de stocker les valeurs intermédiaires à chaque appel pour effectuer la multiplication une fois que l'appel a retourné une valeur.

Une façon de transformer ce code en \textbf{récursivité terminale} (\textit{tail recursion}) est d'utiliser un accumulateur :

\begin{minted}{python}
def facto_TR(n, accu=1):
  if n <= 1 :     # Cas de base :
    return accu
  else :          # Cas général
    return facto_TR_(n - 1, n * accu)
\end{minted}

Observons les calculs effectués pour voir la différence :

$$\begin{aligned}
    facto(4) & = facto(3, 4 \times 1)  \\
             & = facto(2, 3 \times 4)  \\
             & = facto(1, 2 \times 12) \\
             & = 24
  \end{aligned}
$$

Le calcul ne \og gonfle \fg pas au fur et à mesure, la mémoire est \og soulagée \fg.

L'un des autres avantages de cette façon de formuler les fonctions récursives et de pouvoir facilement les transformer en fonctions \textit{itératives},
en boucle \py{Pour}. Par exemple :

\begin{minted}{python}
def facto_ITER(n, accu=1):
  accu = 1
  for i in range(1, n+1) :
    accu *= i
  return accu
\end{minted}

On retrouve bien notre accumulateur.

\textit{Remarques :}
\begin{itemize}
  \item certains langages de programmation (Scala par exemple) peuvent optimiser les fonctions récursives terminales lors de la compilation en les transformant en fonctions itératives. Ce n'est pas le cas de \python
  \item dans certains langages (Haskell, Javascript...), lorsque le compilateur \og comprend \fg\ que l'appel récursif à effectuer est terminal, la fonction appelante est retirée de la pile d'exécution et remplacée par le nouvel appel.
        Voir cet article de \href{https://en.wikipedia.org/wiki/Tail_call}{wikipedia}.
\end{itemize}

\section{Applications}

\subsection{Parcours en profondeur d'abord}

L'une des utilisations classique de la récursivité est le parcours de graphe (ou d'arbre) en profondeur d'abord.

On rappelle que dans le cas d'un graphe, un parcours en profondeur d'abord issu d'un sommet va suivre un chemin de proche en proche
(de voisin en voisin) sans jamais visiter un sommet déjà rencontré. Lorsque l'on se trouve dans la situation où un sommet
n'a aucun voisin ou que des voisins déjà visités, on arrête le parcours de cette branche et l'on \og remonte \fg\ au précédent sommet.

Par exemple pour la figure \ref{deuxGraphes}, les parcours issus du sommet $A$ sont (si l'on a plusieurs choix de voisins, on les visite dans l'ordre lexicographique):

\begin{itemize}
  \item Graphe de gauche : $A \rightarrow C \rightarrow D \rightarrow E \rightarrow G \rightarrow B \rightarrow F$
  \item Graphe de droite : $A \rightarrow B \rightarrow D \rightarrow C \rightarrow E$
\end{itemize}

\begin{figure}[ht!]
  \centering
  \begin{tikzpicture}[node distance=1.5cm]
    \tikzstyle{every node}=[shape=circle,draw=black]
    \node (A) {A};
    \node[below right of=A] (C) {C};
    \node[above right of=C] (D) {D};
    \node[above of=D] (E) {E};
    \node[below of=C] (G) {G};
    \node[below left of=G] (F) {F};
    \node[below right of=G] (B) {B};

    \draw (A) -- (C);
    \draw (C) -- (D);
    \draw (D) -- (E);
    \draw (C) -- (G);
    \draw (G) -- (F);
    \draw (G) -- (B);

    \node [right of=C, node distance=3cm] (Abis) {A};
    \node[right of=Abis, node distance=2cm] (Bbis) {B};
    \node[right of=Bbis, node distance=2cm] (Dbis) {D};
    \node[below right of=Abis, node distance=2cm] (Cbis) {C};
    \node[right of=Cbis, node distance=2cm] (Ebis) {E};

    \draw (Abis) -- (Bbis);
    \draw (Abis) -- (Cbis);
    \draw (Bbis) -- (Dbis);
    \draw (Cbis) -- (Dbis);
    \draw (Cbis) -- (Ebis);
    \draw (Dbis) -- (Ebis);
  \end{tikzpicture}
  \caption{Deux graphes}
  \label{deuxGraphes}
\end{figure}

L'algorithme récursif d'un tel parcours est le suivant :

\begin{minted}{text}
Fonction parcours(sommet, graphe):
    Si sommet n'a pas été visité:
        Visiter sommet
        Marquer sommet comme visité
        Pour tous les voisins de sommet dans graphe:
            Si voisin n'a pas été visité:
                parcours(voisin, graphe)
\end{minted}

L'implémentation en \python\ soulève deux problèmes :
\begin{itemize}
  \item comment garder trace des sommets visités ?
  \item quelle forme donner au résultat du parcours ? S'agit-il simplement d'afficher les sommets dans l'ordre de leur parcours ou faut-il effectuer des actions pour chacun d'eux ?
\end{itemize}

Une solution au premier problème peut être de mettre à jour une liste ou un ensemble contenant les sommets déjà visités (ou leur nom si ceux-ci sont tous différents).
La structure de liste est au programme de NSI mais n'est pas la plus adaptée dans le cadre d'un test d'appartenance tel que \py{voisin in sommets_visites}. 
En effet le coût d'une telle instruction est linéaire en la taille de la liste.
La structure d'ensemble est plus adaptée (test d'appartenance en coût constant) mais cette structure n'est pas au programme de NSI.

Optons ici pour une liste. Celle-ci sera crée lors du premier appel de la fonction récursive et transmise comme paramètre lors des différents appels.
L'aspect mutable des listes permet d'assurer que, lors de chaque appel, la liste transmise en paramètre sera identique.

Concernant la deuxième question, on procèdera de la même façon avec une liste contenant les noms des différents sommets visités dans l'ordre du parcours.
Dans le cas présent, la liste contenant le nom des sommets visités rempli cet office !

Une implémentation \python\ peut donc être :

\begin{minted}{python}
def parcours_profondeur(sommet, graphe, sommets_visites=None):
    if sommets_visites is None:
        sommets_visites = []

    if sommet not in sommets_visites:
        sommets_visites.append(sommet)
        for voisin in graphe[sommet]:
            if voisin not in sommets_visites:
                parcours_profondeur(voisin, graphe, sommets_visites)
    return sommets_visites
\end{minted}

\textit{Remarques :}
\begin{itemize}
  \item Dans la fonction précédente, le graphe est représenté en machine par une liste d'adjacences : \py{grahe = {sommet: [liste des voisins de sommet]}}
  \item Le marquage des sommets visités pourrait être traité différemment. Il est par exemple possible, dans le paradigme de la programmation orientée objet,
        d'ajouter un attribut \py{visité} de type booléen à la classe \py{Sommet}
\end{itemize}

\subsection{Récursif \textit{vs} Itératif}

Certains problèmes algorithmiques se prêtent bien à une résolution algorithmique, l'algorithme est clair et rapide à écrire.
Toutefois, son exécution par \python\ peut être laborieuse.

Continuons d'étudier le parcours en profondeur d'abord, cette fois ci dans le cadre d'un grand graphe (900 sommets, 300 000 arêtes !).
Pour un tel graphe généré aléatoirement le temps d'exécution de l'appel \py{parcours_profondeur("0", graphe)} est de plus de 6 secondes !

Lors de chaque appel récursif, un sommet est visité. Le graphe étant connexe, tous les sommets sont visités et on compte donc 900 appels récursifs.
Pour chacun d'entre eux, \python\ ajoute une \textit{frame} à la pile d'exécution. Cette \textit{frame} contient différentes informations sur l'appel décrit
(portion de code concernée, valeur des variables...). Cet empilement de \textit{frame} explique le grand temps d'exécution.

Il est aussi intéressant de mesurer la profondeur de la pile d'appels. Imaginons à ce titre un graphe étoilé de 900 sommets, dans lequel un unique sommet est
relié au 899 autres, ceux-ci n'ayant pas d'autre voisin. Le parcours en profondeur issu du sommet central effectuera bien 900 appels récursifs (l'appel
initial suivi du parcours des 899 voisins) mais la pile d'exécution ne dépassera jamais la hauteur de 2 : l'appel initial et un autre appel qui sera dépilé après
son exécution car les « voisins » du sommet central n'ont pas d'autres voisins à visiter.

À l'inverse, dans le cas d'un graphe complet (chaque sommet est voisin de tous les autres), la pile d'appels sera aussi haute que le graphe compte de sommets. Cela peut
rapidement devenir problématique, \python\ n'autorisant par défaut que des piles d'appels de hauteur 1 000 au maximum.

Comment s'affranchir d'une telle consommation de ressources ? L'idéal est de sortir d'une approche récursive pour rédiger un algorithme itératif.
La conversion pour le parcours en profondeur d'abord est simple à réaliser : on utilise une pile gardant trace des sommets à étudier dans l'ordre de leur rencontre.

\begin{minted}{text}
Fonction parcours_profondeur_iteratif(sommet, graphe):
    Créer une pile vide
    Empiler le sommet
    Tant que la pile est non vide:
        Dépiler un sommet
        Si sommet n'a pas été visité:
            Visiter sommet
            Pour chaque voisin de sommet dans graphe:
                Si voisin n'a pas été visité:
                    Empiler voisin
\end{minted}

L'implémentation en \python\ est donnée ci-dessous :

\begin{minted}{python}
def parcours_profondeur_iter(sommet, graphe):
    sommets_visites = set()
    ordre = []
    pile = [sommet]
    while pile:
        sommet = pile.pop()
        if sommet not in sommets_visites:
            sommets_visites.add(sommet)
            ordre.append(sommet)
            for voisin in graphe[sommet]:
                if voisin not in sommets_visites:
                    pile.append(voisin)
    return ordre  
\end{minted}

On notera l'utilisation de deux structures distinctes pour gérer l'ordre du parcours et les sommets visités : l'ordre est stocké dans une liste
qui assure que la \og chronologie \fg\ du parcours est bien conservée. Les sommets visités sont quant à eux stockés dans un ensemble ce qui permet de tester
efficacement le fait qu'un sommet ait été visité ou non.

L'exécution de ce code sur le même graphe que précédemment ne prend que 0,1 s !

\subsection{Redondance des calculs}

Considérons l'algorithme classique permettant de calculer les termes de la suite de Fibonacci :

$$F_0 = 0\,;\,F_1=1$$
$$\forall n \in \mathbb{N}, F_{n+2}=F_{n+1}+F_{n}$$

\begin{minted}{python}
def fibo_rec(n):
    if n <= 1:
        return n
    else:
        return fibo_rec(n-1) + fibo_rec(n - 2)
\end{minted}

Comptons comme précédemment le nombre d'appels récursifs. Pour $n=10$, on en obtient ... 177 !

Pourquoi cette explosion du nombre d'appels ? Détaillons les calculs :

\begin{align*}
  F_{10} & =  F_9 + F_8                           \\
         & =  F_8 + F_7 + F_7+F_6                 \\
         & =  F_7+F_6 + F_6+F_5 + F_6+F_5+F_5+F_4 \\
         & =...
\end{align*}


\begin{figure}[!ht]
  \begin{center}
    \begin{tikzpicture}[
        level distance=0.8cm,
        level 1/.style={sibling distance=4cm},
        level 2/.style={sibling distance=2cm},
        level 3/.style={sibling distance=1cm},
      ]
      \tikzstyle{every node}=[draw=none, circle, minimum size=3mm, inner sep=0mm]

      \node (R) at (0,0) {$F_{10}$}
      child {node {$F_9$}
          child {node {$F_8$}
              child {node {$F_7$}
                  child {node {$\dots$}}
                }
              child {node {$F_6$}
                  child {node {$\dots$}}
                }
            }
          child {node {$F_7$}
              child {node {$F_6$}
                  child {node {$\dots$}}
                }
              child {node {$F_5$}
                  child {node {$\dots$}}
                }
            }
        }
      child {node {$F_8$}
          child {node {$F_7$}
              child {node {$F_6$}
                  child {node {$\dots$}}
                }
              child {node {$F_5$}
                  child {node {$\dots$}}
                }
            }
          child {node {$F_6$}
              child {node {$F_5$}
                  child {node {$\dots$}}
                }
              child {node {$F_4$}
                  child {node {$\dots$}}
                }
            }
        };
    \end{tikzpicture}
  \end{center}
  \caption{Une partie des appels récursifs effectués par \py{fibo_rec(10)}}
\end{figure}


Comme on peut le voir, certains appels sont répétés ($F_7$ par exemple) : \python\ va calculer plusieurs fois leur valeur.

Cette redondance des calculs est classique lorsque l'on utilise la récursivité. Il existe deux techniques permettant d'y pallier :
\begin{itemize}
  \item Utiliser un code itératif (à nouveau !),
  \item ne pas recalculer les valeurs déjà calculées.
\end{itemize}

La deuxième méthode nécessite donc de garder la trace des valeurs déjà calculées. On parle de « mémoïsation ». Cela peut par exemple
passer par l'utilisation d'une liste permettant de stocker les résultats intermédiaires :

\begin{minted}{python}
def fibo_memo(n, F=None):
    if F is None:
        F = [None] * (n + 1)
        F[0] = 0
        F[1] = 1

    if F[n] is None:
        F[n] = fibo_memo(n - 1, F) + fibo_memo(n - 2, F)
    return F[n]
\end{minted}

Le calcul de \py{fibo_memo(10)} effectue désormais 19 appels correspondant à ceux de l'arbre de la figure \ref{fibo-mem}.

\begin{figure}[H]
  \begin{center}
    \begin{tikzpicture}[
        level distance=0.8cm,
        level 1/.style={sibling distance=2cm},
        level 2/.style={sibling distance=2cm},
        level 3/.style={sibling distance=2cm},
      ]
      \tikzstyle{every node}=[draw=none, circle, minimum size=3mm, inner sep=0mm]

      \node (R) at (0,0) {$F_{10}$}
      child {node {$F_9$}
          child {node {$F_8$}
              child {node {$F_7$}
                  child {node {$F_6$}
                      child {node {$F_5$}
                          child {node {$F_4$}
                              child {node {$F_3$}
                                  child {node {$F_2$}
                                      child {node {$F_1$}}
                                      child {node {$F_0$}}
                                    }
                                  child {node {$F_1$}}
                                }
                              child {node {$F_2$}}
                            }
                          child {node {$F_3$}}
                        }
                      child {node {$F_4$}}
                    }
                  child {node {$F_5$}}
                }
              child {node {$F_6$}}
            }
          child {node {$F_7$}}
        }
      child {node {$F_8$}};
    \end{tikzpicture}
  \end{center}
  \caption{Les 19 appels récursifs effectués par \py{fibo_mem(10)}}
  \label{fibo-mem}
\end{figure}

Cette approche de mémoïsation est couramment utilisée dans le cadre de la \textit{Programmation dynamique}.

La solution itérative est encore plus légère :

\begin{minted}[samepage]{python}
def fibo_iter(n):
    F = [0, 1]

    while len(F) <= n:
        F.append(F[-1] + F[-2])

    return F[n]
\end{minted}
\end{document}